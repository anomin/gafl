library ieee;
use ieee.std_logic_1164.all;

entity tb is
end entity tb;

architecture behavior of tb is
	component main
		port(clk : in std_logic;
			 rst : in std_logic);
	end component main;
	signal clk : std_logic := '0';
	signal rst : std_logic := '0';
	
	constant clk_period : time := 1 ns;
begin

uut: main PORT map(
	clk => clk,
	rst => rst
);

   clk_process :process
   begin
        clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;

	stim_proc: process
	begin
		rst<='1';
		wait for 3 ns;
		rst<='0';
		wait;
	end process;
end architecture behavior;
