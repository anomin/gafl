
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use std.standard.all;

--library ieee_proposed;
--use ieee_proposed.float_pkg.all;

entity main is
	port (
		clk : in std_logic; --input clock
		rst : in std_logic --input reset(active on 1)
	);
end entity main;

architecture RTL of main is
	--BEGIN define constants in here
	constant n: integer:=4; -- how much population
	constant c_min_x: integer:=0; --minimum value in f(x)
	constant c_max_x: integer:=5; --maximum valuie in f(x)
	constant c_min_y: integer:=0; --minimum value in f(y)
	constant c_max_y: integer:=25; --maximum value in f(y)
	constant L: integer:=6; --number of bits per string(base)
	constant max_base: integer:= 2 ** L;
	constant rel_fitness : real := 0.75; --relative fitness for selection
	
	
	type trd_x_t is array (0 to c_max_x) of integer range 0 to 5;
	constant TRDx: trd_x_t:=(0,1,2,3,4,5);	-- training data
	type trd_y_t is array(0 to c_max_x) of integer range 0 to 25;
	constant TRDy:trd_y_t:=(0,1,4,9,16,25);	-- training data
	--END define constants
	
	
	-- BEGIN type definitions
	type population_matrix_t is array (0 to n-1) of unsigned( 4*L-1 downto 0);
	type column_vector_t is array (0 to n-1) of real; --fitness vector type
	type matrix_vector_t is array (0 to c_max_x) of column_vector_t; --used in fitness calculus
	type state_t is (idle,init_population, fitness, selection, crossover, mutation, end_ga);
	-- END type definitions
	
	-- BEGIN define signals
 	signal current_state_s: state_t :=idle;
	--END define signals
--initialization function is used for first initialization of population
function initialization (current_state: state_t )
return population_matrix_t is 
variable seed1: positive:=100;
variable seed2: positive:=3000;
variable rand_base1: real:=0.0;
variable rand_base2: real:=0.0;
variable rand_base3: real:=0.0;
variable rand_base4: real:=0.0;
variable population: population_matrix_t; --return this
begin
	if current_state=init_population then
		for i in 0 to n-1 loop
			uniform(seed1,seed2,rand_base1); --random generation of population
			uniform(seed1,seed2,rand_base2);
			uniform(seed1,seed2,rand_base3);
			uniform(seed1,seed2,rand_base4);
			population(i):=to_unsigned(integer(trunc(rand_base1*real(max_base))),L)&to_unsigned(integer(trunc(rand_base2*real(max_base))),L)&to_unsigned(integer(trunc(rand_base3*real(max_base))),L)&to_unsigned(integer(trunc(rand_base4*real(max_base))),L);
--		population(0):="000111010100010110110011"; --example
--		population(1):="010010001100101100100110";
--		population(2):="010101101010001101101000";
--		population(3):="100100001001101100100011";
		end loop;			
	end if;
	return population;
end initialization;
	
-- fitness function is for calculation of fitness vector(how adaptable is
-- each string). Implemented from Fuzzy Logic with Engineering Applications
-- By Timothy J. Ross, Chapter 6.
function fitness (current_state: state_t; population: population_matrix_t)
return column_vector_t is
variable base1 : column_vector_t:= (others=>0.0);
variable base2 : column_vector_t:= (others=>0.0);
variable base3 : column_vector_t:= (others=>0.0);
variable base4 : column_vector_t:= (others=>0.0);
variable y_prime: matrix_vector_t:=(others=>(others=>0.0));
variable error: column_vector_t:= (others=>0.0);
variable aux_sum: real:=0.0;
variable sum: real:=0.0;
variable average: real;
variable max_index : integer := 0;
variable fitness_v : column_vector_t:= (others=>0.0);

begin
if current_state=fitness then
--dividing each string in bases. base1 and base2 is for x funtion
-- base3 and base4 is for y function
	for i in 0 to n-1 loop
--		aux1:=real(to_integer('0'&population_s(i)(4*L-1 downto 3*L)));
--		aux2:=real(( 2 ** L) - 1);
--		aux3:=real(c_max_x-c_min_x);
--		base1(i):=aux1/aux2*aux3;
		base1(i):= real(c_min_x) + real(to_integer('0'&population(i)(4*L-1 downto 3*L)))/real(( 2 ** L) - 1)*real(c_max_x-c_min_x);
		base2(i):= real(c_min_x) + real(to_integer('0'&population(i)(3*L-1 downto 2*L)))/real(( 2 ** L) - 1)*real(c_max_x-c_min_x);
		base3(i):= real(c_min_y) + real(to_integer('0'&population(i)(2*L-1 downto L)))/real(( 2 ** L) - 1)*real(c_max_y-c_min_y);
		base4(i):= real(c_min_y) + real(to_integer('0'&population(i)(L-1 downto 0)))/real(( 2 ** L) - 1)*real(c_max_y-c_min_y);
		--base1(i):= real (c_min_x + (population_s(i)(4*L-1 downto 3*L)/((2 srl L) - 1))*(c_max_x-c_min_x));
		--base2(i):= c_min_x + (population_s(i)(3*L-1 downto 2*L)/(2<<L-1))*(c_max_x-c_min_x);
		--base3(i):= c_min_y + (population_s(i)(2*L-1 downto L)/(2<<L-1))*(c_max_y-c_min_y);
		--base4(i):= c_min_y + (population_s(i)(L-1 downto 0)/(2<<L-1))*(c_max_y-c_min_y);
	end loop;
	--chechinkg out the y` for error calculus
	for i in 0 to n-1 loop
		for j in 0 to c_max_x loop
			-- we are calculationg for each valuie in x training data
			--treating 2 cases, if base1 is in training data
			-- and if base2 is in training data
			if real(j) < base1(i) then
				
				
				y_prime(j)(i):=base3(i)*(real(j)/(base1(i)));
				
--				y_temp_in_X_on_Small=(-j)/(base1(i))+1;
--				--below,basicly it is X in f_y
--				y_prime(i)(j)=-(base3(i)*(y_temp_in_X-1));--corespondenta in functia y on L
			end if;
			
			if real(j)>(real(c_max_x)-base2(i)) then 
							--y_temp_in_X_on_Large=real(j-c_max_x)/base2(i)+1;
				y_prime(j)(i):=base4(i)*(real(j-c_max_x)/base2(i))+real(c_max_y);				--corespondenta in functia y on VL
--				y_temp_in_X_on_Large=(c_max_x-j)/(-base2(i))+1;
--				y_prime(i)(j)=-((-base4(i))*y_temp-1)+1;				--corespondenta in functia y on VL
			end if;
		end loop;
	end loop;
	--square error given formula from book
	for i in 0 to n-1 loop
	aux_sum:=0.0;
		for j in 0 to c_max_x loop
			aux_sum:=aux_sum + (real(TRDy(j))-y_prime(j)(i))**2;
		end loop;
	error(i):=real(1000) - aux_sum;
	sum:=sum+error(i);
	if error(max_index) < error(i) then
		max_index:=i;
	end if;
	end loop;
	average:=sum/real(c_max_x-1); --number of Training Datas
	--finaly, filling fitness vector with relative errors
	for i in 0 to n-1 loop
		fitness_v(i):=error(i)/average;
	end loop;
end if;
return fitness_v;
end fitness;

-- for selection function, given the fitness and population we do selection
-- note: this selection works by comparing to a relative fitness
-- and substitute the weak string with the bestest fitesst
-- IT IS NOT PROBABILISTIC!!(note to self: make a roullete or something)
function selection (current_state:state_t; fitness: column_vector_t; population: population_matrix_t)
return population_matrix_t is
variable population_v: population_matrix_t;
variable max: integer:=0;
begin
if current_state=selection then
	for i in 0 to n-1 loop
		if fitness(i) > fitness(max) then
			max:=i; -- best fittest index
		end if;	
	end loop;
	
	for i in 0 to n-1 loop
		--if lower, substitute
		if fitness(i) < rel_fitness then
			population_v(i):=population(max);
		else
			population_v(i):=population(i);
		end if;	
	end loop;
end if;
return population_v;
end selection;

--single point crossover
function crossover (current_state:state_t; population: population_matrix_t)
return population_matrix_t is 
variable seed1:integer:=450;
variable seed2:integer:=934;
variable rand_gen:real:=0.0;
variable rand:integer:=0;
variable population_v : population_matrix_t;

begin
if current_state=crossover then
	for i in 0 to n-1 loop
		uniform(seed1,seed2,rand_gen);
		rand:=integer(rand_gen*24.0); -- generating and converting crossover point
		-- swap(final string is: (random till 0) concatentate (4L till random-1) )
		population_v(i):=population(i)(rand-1 downto 0) & population(i)(4*L-1 downto rand);
	end loop;
end if;
return population_v;
end crossover;
-- mutation of 1 bit of random choose string(to ensure divercity)
-- basic ideea:
--chooese m individuals from population
--generate random number
--not population(i)(random_number)
function mutation(current_state: state_t; population:population_matrix_t)
return population_matrix_t is
variable seed1:integer:=450;
variable seed2:integer:=934;
variable rand_mut: real:=0.0; --random mutation
variable rand_ch: real:=0.0;  --random choose
variable population_v : population_matrix_t;
begin
if current_state=mutation then
for i in 0 to n-1 loop
	--generate random choose
	uniform(seed1,seed2,rand_ch);
	if rand_ch<=0.5 then --for the sake of because I can
		--generate random mutation position
		uniform(seed1,seed2,rand_mut);
		population_v(i):= population(i)(4*L-1 downto integer(rand_mut*24.0)+1 ) & not population(i)( integer(rand_mut*24.0) ) &	 population(i)( integer(rand_mut*24.0)-1 downto 0); --think
	else
		population_v(i):=population(i);
	end if;

end loop;

end if;
return population_v;
end mutation;

--------------------------------------------------------------------------		
begin

main:process(rst,clk)
variable next_state: state_t;
begin
if rising_edge(clk) then
	if rst/='0' then
		next_state:=init_population;
	else
	-- GENETIC STATE MACHINE
		case current_state_s is --think final state
			when init_population => next_state:=fitness;
			when fitness => next_state:=selection;
			when selection => next_state:=crossover;
			when crossover => next_state:=mutation;
			when mutation => next_state:=fitness;
			when others => null;
		end case;
		
	end if;
	current_state_s<=next_state;
end if;
end process;

gafl: process(current_state_s)
variable population_v:population_matrix_t;
variable fitness_v : column_vector_t;

begin

case current_state_s is --think final state
--executing functions for each state
--to ensure correct execution, current state must match function state
--that is why current_state is passed
			when init_population => population_v:=initialization(current_state_s);
			when fitness => fitness_v:=fitness(current_state_s, population_v);
			when selection => population_v:=selection(current_state_s, fitness_v, population_v);
			when crossover => population_v:=crossover(current_state_s, population_v);
			when mutation => population_v:=mutation(current_state_s, population_v);
			when others => null;
		end case;
end process;



--initialization1: process (rst, current_state_s)
--variable seed1: positive:=100;
--variable seed2: positive:=3000;
--variable rand_base1: real:=0.0;
--variable rand_base2: real:=0.0;
--variable rand_base3: real:=0.0;
--variable rand_base4: real:=0.0;
--begin
--	if current_state_s=init_population then
--		for i in 0 to n-1 loop
----			uniform(seed1,seed2,rand_base1);
----			uniform(seed1,seed2,rand_base2);
----			uniform(seed1,seed2,rand_base3);
----			uniform(seed1,seed2,rand_base4);
----			population_s(i)<=to_unsigned(integer(trunc(rand_base1*real(max_base))),L)&to_unsigned(integer(trunc(rand_base2*real(max_base))),L)&to_unsigned(integer(trunc(rand_base3*real(max_base))),L)&to_unsigned(integer(trunc(rand_base4*real(max_base))),L);
--		population_s(0)<="000111010100010110110011";
--		population_s(1)<="010010001100101100100110";
--		population_s(2)<="010101101010001101101000";
--		population_s(3)<="100100001001101100100011";
--		end loop;			
--	end if;
--end process;
--
--fitness_function:process(current_state_s)
--variable base1 : column_vector_t:= (others=>0.0);
--variable base2 : column_vector_t:= (others=>0.0);
--variable base3 : column_vector_t:= (others=>0.0);
--variable base4 : column_vector_t:= (others=>0.0);
--variable y_prime: matrix_vector_t:=(others=>(others=>0.0));
--variable error: column_vector_t:= (others=>0.0);
--variable aux_sum: real:=0.0;
--variable sum: real:=0.0;
--variable average: real;
--variable max_index : integer := 0;
--variable fitness_v : column_vector_t:= (others=>0.0);
--
--begin
--if current_state_s=fitness then
--	for i in 0 to n-1 loop
----		aux1:=real(to_integer('0'&population_s(i)(4*L-1 downto 3*L)));
----		aux2:=real(( 2 ** L) - 1);
----		aux3:=real(c_max_x-c_min_x);
----		base1(i):=aux1/aux2*aux3;
--		base1(i):= real(c_min_x) + real(to_integer('0'&population_s(i)(4*L-1 downto 3*L)))/real(( 2 ** L) - 1)*real(c_max_x-c_min_x);
--		base2(i):= real(c_min_x) + real(to_integer('0'&population_s(i)(3*L-1 downto 2*L)))/real(( 2 ** L) - 1)*real(c_max_x-c_min_x);
--		base3(i):= real(c_min_y) + real(to_integer('0'&population_s(i)(2*L-1 downto L)))/real(( 2 ** L) - 1)*real(c_max_y-c_min_y);
--		base4(i):= real(c_min_y) + real(to_integer('0'&population_s(i)(L-1 downto 0)))/real(( 2 ** L) - 1)*real(c_max_y-c_min_y);
--		--base1(i):= real (c_min_x + (population_s(i)(4*L-1 downto 3*L)/((2 srl L) - 1))*(c_max_x-c_min_x));
--		--base2(i):= c_min_x + (population_s(i)(3*L-1 downto 2*L)/(2<<L-1))*(c_max_x-c_min_x);
--		--base3(i):= c_min_y + (population_s(i)(2*L-1 downto L)/(2<<L-1))*(c_max_y-c_min_y);
--		--base4(i):= c_min_y + (population_s(i)(L-1 downto 0)/(2<<L-1))*(c_max_y-c_min_y);
--	end loop;
--	--aflarea y-ului
--	for i in 0 to n-1 loop
--		for j in 0 to c_max_x loop
--			if real(j) < base1(i) then
--				
--				
--				y_prime(j)(i):=base3(i)*(real(j)/(base1(i)));
--				
----				y_temp_in_X_on_Small=(-j)/(base1(i))+1;
----				--below,basicly it is X in f_y
----				y_prime(i)(j)=-(base3(i)*(y_temp_in_X-1));--corespondenta in functia y on L
--			end if;
--			
--			if real(j)>(real(c_max_x)-base2(i)) then 
--							--y_temp_in_X_on_Large=real(j-c_max_x)/base2(i)+1;
--				y_prime(j)(i):=base4(i)*(real(j-c_max_x)/base2(i))+real(c_max_y);				--corespondenta in functia y on VL
----				y_temp_in_X_on_Large=(c_max_x-j)/(-base2(i))+1;
----				y_prime(i)(j)=-((-base4(i))*y_temp-1)+1;				--corespondenta in functia y on VL
--			end if;
--		end loop;
--	end loop;
--	--eroarea patratica
--	for i in 0 to n-1 loop
--	aux_sum:=0.0;
--		for j in 0 to c_max_x loop
--			aux_sum:=aux_sum + (real(TRDy(j))-y_prime(j)(i))**2;
--		end loop;
--	error(i):=real(1000) - aux_sum;
--	sum:=sum+error(i);
--	if error(max_index) < error(i) then
--		max_index:=i;
--	end if;
--	end loop;
--	average:=sum/real(c_max_x-1); --number of Training Datas
--	
--	for i in 0 to n-1 loop
--		fitness_v(i):=error(i)/average;
--	end loop;
--
--	fitness_s<=fitness_v;
--end if;
--end process;
--
--selection_function: process(current_state_s)
--variable population_v: population_matrix_t;
--variable max: integer:=0;
--begin
--if current_state_s=selection then
--	for i in 0 to n-1 loop
--		if fitness_s(i) > fitness_s(max) then
--			max:=i;
--		end if;	
--	end loop;
--	
--	for i in 0 to n-1 loop
--		if fitness_s(i) < rel_fitness then
--			population_v(i):=population_s(max);
--		else
--			population_v(i):=population_s(i);
--		end if;	
--	end loop;
--	--population_s<=population_v;
--end if;
--end process selection_function;
--
----single point crossover
--crossover_function:process(current_state_s)
--variable seed1:integer:=450;
--variable seed2:integer:=934;
--variable rand_gen:real:=0.0;
--variable rand:integer:=0;
--variable population_v : population_matrix_t;
--
--begin
--if current_state_s=crossover then
--	for i in 0 to n-1 loop
--		uniform(seed1,seed2,rand_gen);
--		rand:=integer(rand_gen*24.0);
--		--generate rand
--		--mask:=others('0');
--		--mask:=mask xor population_s(i)(4*L-1 downto rand);
--		--mask:=mask srl rand; --shift right must be 
--		population_v(i):=population_s(i)(rand-1 downto 0) & population_s(i)(4*L-1 downto rand);
--		--population_v(i)(4*L-1 downto 4*L-rand-1):=population_s(i)(4*L-1 downto rand);
--		--populatio
--		--population_s(i)(4*L-1 downto rand) --make swap and shit
--	end loop;
--	population_s<=population_v;
--end if;
--end process;
--
--mutation_function:process(clk)
--variable rand: integer:=4;
--variable rand_ch: std_logic:='1';
--variable population_v : matrix_vector_t;
--begin
--population_v:=population_s;
--for i in 0 to n-1 loop
--	--generate rand_ch
--	if rand_ch='1' then 
--		--generate rand
--		not population_v(i)(rand); --think
--	end if;
--end loop;
--	--chooese m individuals from population
--	--generate random number
--	--not population(i)(random_number)
--	population_s<=popualtion_v;
--end process;

end architecture RTL;
